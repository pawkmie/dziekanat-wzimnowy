﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DziekanatWzim.Models
{
    public class Duty
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Godziny { get; set; }
    }
}