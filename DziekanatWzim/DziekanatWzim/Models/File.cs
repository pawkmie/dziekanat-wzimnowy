﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DziekanatWzim.Models
{
    public class File
    {
        public int Id { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
    }
}